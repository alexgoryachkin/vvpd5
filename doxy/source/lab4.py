import sys


def check_input(inp):
"""
Функция проверки ввода.

Проверяет полученную строку на бинарность.

Аргументы:
    inp    Бинарная строка. Функция должна принимать строку, состоящую только из 0 и 1 без разделителей.

Возвращает:
    True или False.
"""
    try:
        temp = inp
        temp = bin(int(temp, 2))[2:]
    except ValueError:
        return False
    return True


def make_decision(inp):
"""
Функция, реализующая алгоритм выполнения задачи.

Это функция реализует алгоритм подсчета количества строк, которое можно составить из полученной на входе бинарной
строки. Учитывается, что составленные строки имеют такое же количество 0 и 1. Если на вход поступила пустая строка,
алгоритм вернет её.

Аргументы:
        inp    Бинарная строка. Функция должна принимать строку, состоящую только из 0 и 1 без разделителей.

Возвращает:
        count    Количество строк, возможных для составления.
"""
    if not inp.strip():
        return inp
    else:
        inp = list(inp)
        zeros = inp.count('0')
        one = inp.count('1')
        minimal = inp
        maximal = minimal
        minimal = sorted(minimal)
        maximal = sorted(maximal, reverse=True)
        maximal = ''.join(maximal)
        minimal = ''.join(minimal)
        maximal = int(maximal, 2)
        minimal = int(minimal, 2)
        count = 0
        for i in range(minimal, maximal + 1):
            temp = bin(i)[2:]
            temp = list(temp)
            if len(temp) < len(inp):
                for j in range(0, len(inp) - len(temp)):
                    temp.insert(j, '0')
            if temp.count('0') == zeros and temp.count('1') == one:
                count += 1
    return count

def main():
"""
Главная функция.

Здесь выполняется логика программы.
"""
    binary = input("Введите строку из 0 и 1:\n")
    passing = check_input(binary)
    if passing:
        print("Число способов:", make_decision(binary))
    else:
        print("Получен неверный тип входных данных."
              "Завершение программы")


if __name__ == '__main__':
    sys.exit(main())

import lab4


def test_rightinp():
"""
Функция, тестирующая проверку ввода.

Здесь проверяется работа проверки ввода при верном вводе данных. Если строка является бинарной - функция проверки ввода
вернет True, в противном случае - False.

Тесты:
        1) строка '100' является бинарной, функция вернет True

        2) строка '111' является бинарной, функция вернет True

        3) строка '10101' является бинарной, функция вернет True
"""
    assert lab4.check_input('100') == True
    assert lab4.check_input('111') == True
    assert lab4.check_input('10101') == True


def test_badinp():
"""
Функция, тестирующая проверку ввода.

Здесь проверяется работа проверки ввода при неверном вводе данных. Если строка является бинарной - функция проверки
ввода вернет True, в противном случае - False.

Тесты:
        1) строка '222' не является бинарной, функция вернет False

        2) строка '' не является бинарной, функция вернет False

        3) строка 'as-=ds' не является бинарной, функция вернет False
"""
    assert lab4.check_input('222') == False
    assert lab4.check_input('') == False
    assert lab4.check_input('as-=ds') == False


def test_goodinp_work():
"""
Функция, тестирующая алгоритм, выполняющий задачу.

Здесь проверяется работа алгоритма при неверном вводе данных. Фунция алгоритма должна подсчитать и вернуть
количество способов составления разных бинарных строк, при условии что 0 и 1 будет столько же сколько и в
первоначальной бинарной строке. Если на вход получена пустая строка, то функция алгоритма вернет эту строку.

Тесты:
        1) строка '100' является бинарной, функция вернет значение 3

        2) строка '111' является бинарной, функция вернет значение 1

        3) строка '10101' является бинарной, функция вернет значение 10
"""
    assert lab4.make_decision('100') == 3
    assert lab4.make_decision('000') == 1
    assert lab4.make_decision('10101') == 10


def test_badinp_work():
"""
Функция, тестирующая алгоритм, выполняющий задачу.

Здесь проверяется работа алгоритма при неверном вводе данных. Фунция алгоритма должна подсчитать и вернуть
количество способов составления разных бинарных строк, при условии что 0 и 1 будет столько же сколько и в
первоначальной бинарной строке. Если на вход получена пустая строка, то функция алгоритма вернет эту строку.

Тесты:
        1) строка '' является пустой, функция вернет значение 3

        2) строка '     ' является пустой, функция вернет '     '
"""
    assert lab4.make_decision('') == ''
    assert lab4.make_decision('     ') == '     '
